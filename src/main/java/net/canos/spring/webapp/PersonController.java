package net.canos.spring.webapp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/person")
public class PersonController {
	Logger log = Logger.getLogger(getClass());
	
	@Autowired
	private PersonService personService;

	@RequestMapping(value="/{personId}",	method	= RequestMethod.GET)
	public Person get(@PathVariable("personId") Integer personId){
		log.info("GET");
		Person person = personService.findById(personId);
		return person;
	}
	
	@RequestMapping(value="/save",	method	= RequestMethod.POST)
	public Person create(Person person){
		log.info("POST");
		Person person_new = personService.create(person);
		return person_new;
	}
	
	@RequestMapping(value="/{personId}/update",	method	= RequestMethod.PUT)
	public Person save(Person person){
		log.info("PUT");
		Person person_new = personService.save(person);
		return person_new;
	}
	
	@RequestMapping(value="/{personId}/delete",	method	= RequestMethod.DELETE)
	public String  delete(@PathVariable("personId") Integer personId){
		log.info("DELETE");
		personService.delete(personId);
		return "ok";
	}

}
